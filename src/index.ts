import axios from 'axios';

const configService = axios.create({
    baseURL: 'https://pokeapi.co/api/v2',
    headers: {
        'accept-language': 'es',
        'Content-Type': 'application/json',
    }
});

let esperado = '';

export let funcion = async() => {
    let l = await configService('/pokemon/ditto', {
        method: 'GET'
    })
    
    console.log(l.data.forms[0].name);
    return l.data.forms[0].name;
}

funcion();